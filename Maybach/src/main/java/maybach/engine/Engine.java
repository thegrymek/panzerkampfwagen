package maybach.engine;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.awt.event.*;

import javax.swing.JPanel;

import maybach.layer.*;
import maybach.object.Shape;

import javax.swing.Timer;
//import org.apache.felix.scr.annotations.Component;
//import org.apache.felix.scr.annotations.Service;

//@Component(name = "Engine")
//@Service
public class Engine extends JPanel implements IEngine {

    private static Engine instance;
    private LinkedList<Layer> layers;

    private boolean isRunning = true;
    private final int DELAY_FPS = 15;

    private Thread thread;

    private TKeyAdapter keylistener;
    private TMouseAdapter mouselistener;

    private Shape shapeFocus = null;
    private boolean isShapeFocus = false;

    private Graphics g = null;

    private boolean isStopped = false;

    public Engine() {
//			thread = new Thread(this);
        // adding focus to keyboard
        setFocusable(true);
        layers = new LinkedList<Layer>();
        keylistener = new TKeyAdapter();
        mouselistener = new TMouseAdapter();
        // adding default layer
        layers.add(new Layer("default"));
        // adding key and mouse
        addKeyListener(keylistener);
        addMouseListener(mouselistener);
        addMouseMotionListener(mouselistener);
        // start
    }

    public static Engine getEngine() {
        if (instance == null) {
            synchronized (Engine.class) {
                if (instance == null) {
                    instance = new Engine();
                }
            }
        }
        return instance;
    }

    public static void refreshEngine() {
        instance = null;
        instance = new Engine();
    }

    // Shape focus
    ////////////////////////////////////////////////
    @Override
    public void setShapeFocus(Shape s) {
        this.shapeFocus = s;
        if (s != null) {
            this.isShapeFocus = true;
        } else {
            this.isShapeFocus = false;
        }
    }

    @Override
    public Shape getShapeFocus() {
        return this.shapeFocus;
    }

    @Override
    public boolean isShapeFocus() {
        return this.isShapeFocus;
    }
    /////////////////////////////////////////////////////////////

    @Override
    public void Start() {
        this.isRunning = true;
        this.run();
    }

    @Override
    public void Stop() {
        this.isRunning = false;
    }

    private int dT = 0;

    @Override
    public void update(long dt) {
        this.dT += dt;
        for (Layer l : layers) {
            if (l.isVisible()) {
                l.update(dt);
            }
        }

//        if (this.dT > 25) {
//            if (this.g != null) {
//                super.paint(g);
//                for (Layer l : layers) {
//                    if (l.isVisible()) {
//                        l.paint(g);
//                    }
//                }
//            }
//            this.dT = 0;
//        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        for (Layer l : layers) {
            if (l.isVisible()) {
                l.paint(g);
            }
        }
    }

    // Shapes
    //////////////////////////////////////////////////////////////////////
    @Override
    public void RegisterShape(Shape s) {
        this.layers.getLast().RegisterShape(s);
    }

    @Override
    public void RegisterShape(Shape s, String layerName) {
        ILayer l = this.getLayer(layerName);
        if (l == null || s == null) {
            return;
        }
        l.RegisterShape(s);
    }

    @Override
    public void UnregisterShape(Shape s) {
        for (Layer l : layers) {
            Shape tmp = l.getShape(s);
            if (tmp != null) {
                l.UnregisterShape(tmp);
                break;
            }
        }
    }

    @Override
    public Shape[] getShapes(int layer) {
        if (layer > layers.size()) {
            return null;
        }
        return layers.get(layer).getShapes();
    }

    @Override
    public Shape[] getAllShapes() {
        LinkedList<Shape> shapes = new LinkedList<Shape>();
        for (Layer l : layers) {
            for (Shape s : l.getShapes()) {
                if (s != null) {
                    shapes.add(s);
                }
            }
        }
        return shapes.toArray(new Shape[shapes.size()]);
    }

    // Layers
    //////////////////////////////////////////////////////////////////////
    @Override
    public void RegisterLayer(Layer l) {
        this.layers.add(l);
    }

    @Override
    public Layer getLayer() {
        return layers.getLast();
    }

    @Override
    public ILayer getLayer(String s) {
        for (ILayer l : layers) {
            if (l.getName() == s) {
                return l;
            }
        }
        return null;
    }

    @Override
    public ILayer getLayer(Shape s) {
        for (ILayer l : layers) {
            Shape shape = l.getShape(s);
            if (shape == s) {
                return l;
            }
        }
        return null;
    }

    @Override
    public ILayer getLayer(int l) {
        if (l >= layers.size()) {
            return null;
        } else {
            return layers.get(l);
        }
    }

    @Override
    public void UnregisterLayer(int c) {
        if (c >= layers.size()) {
            return;
        }
        layers.remove(c);
    }

    @Override
    public void translate(int dx, int dy) {
        for (Shape s : this.getAllShapes()) {
            s.translate(dx, dy);
        }
    }

    public void clean() {
        for (Layer l : this.layers) {
            l.clean();
        }
    }

    @Override
    public void run() {
        final Timer timer = new Timer(10, null);
        final Engine thisEngine = this;
        timer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (isRunning) {
                    long cycleTime = System.currentTimeMillis();
                    long difference = 10;
                    thisEngine.update(difference);
                    if (thisEngine.isShapeFocus()) {
                        int x = (int) thisEngine.shapeFocus.x;
                        int y = (int) thisEngine.shapeFocus.y;

                        int centerX = (int) thisEngine.getWidth() / 2;
                        int centerY = (int) thisEngine.getHeight() / 2;

                        int diffx = x - centerX;
                        int diffy = y - centerY;
                        thisEngine.translate(-diffx, -diffy);
                    }
                    thisEngine.repaint();
//                    thisEngine.clean();
                } else {
                    timer.stop();
                }
            }
        });
        timer.start();
    }

    private class TMouseAdapter extends MouseAdapter implements MouseMotionListener {
    }

    private class TKeyAdapter extends KeyAdapter {
    }

    @Override
    public void setBackground(Color BLACK) {
        super.setBackground(BLACK);
    }

    @Override
    public void addMouseListener(MouseListener sel) {
        super.addMouseListener(sel);
    }

    @Override
    public void addMouseMotionListener(MouseMotionListener sel) {
        super.addMouseMotionListener(sel);
    }

    @Override
    public void addMouseWheelListener(MouseWheelListener sel) {
        super.addMouseWheelListener(sel);
    }
}
