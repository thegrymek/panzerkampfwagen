package maybach.object;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class Text2D extends Shape {

	private String text;
	
	public Text2D(int x, int y, String text)
	{
		super(x,y);
		this.name = "Text2D";
		this.text = text;
	}
	
	public void setText(String t)
	{
		this.text = t;
	}
	
	public String getText()
	{
		return this.text;
	}
	
	@Override
	public void paint(Graphics g)
	{
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawString(this.text, this.x, this.y);
	}
}
