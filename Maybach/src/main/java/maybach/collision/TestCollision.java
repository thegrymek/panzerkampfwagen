package maybach.collision;

import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class TestCollision {

	// return true when point is in polygon
	public static boolean pointInPolygon(List<Point> polygon, Point test)
	{
		if ( polygon == null || polygon.size() == 0)
		{
			System.out.println("null");
			return false;
		}
		int nvert = polygon.size();
		int i, j;
		boolean c = false;
		for (i = 0, j = nvert-1; i < nvert; j = i++) {
			Point pi = polygon.get(i);
			Point pj = polygon.get(j);
			if ( ((pi.y>test.y) != (pj.y>test.y)) &&
					(test.x < (pj.x-pi.x) * (test.y-pi.y) / (pj.y-pi.y) + pi.x) )
				c = !c;
		}
		return c;
	}
	
	// return true when one of test point is inside polygon
	public static boolean polygonInPolygon(List<Point> polygon, List<Point> test)
	{
		if ( polygon == null || test == null || polygon.size() == 0 || test.size() == 0)
		{
			return false;
		}
		for ( Point p : test)
		{
			if ( TestCollision.pointInPolygon(polygon, p) )
			{
				return true;
			}
		}
		return false;
	}
	
	public static void updateSquareKollision(List<Point> KollisionPunkt, Point object, double angle, Image img, boolean center)
	{
		if ( KollisionPunkt.size() != 4 || img == null)
		{
			System.out.println("Koll.size = "+ KollisionPunkt + " img= " + img.getWidth(null));
			return;
		}
		int distx = img.getWidth(null);
		int disty = img.getHeight(null);
		// translate to x y
		// x:- y:+ 
		KollisionPunkt.get(0).x = object.x;
		KollisionPunkt.get(0).y = object.y;
		// x:+ y:+
		KollisionPunkt.get(1).x = object.x+distx;
		KollisionPunkt.get(1).y = object.y;
		// x:+ y:-
		KollisionPunkt.get(2).x = object.x+distx;
		KollisionPunkt.get(2).y = object.y+disty;
		// x:- y:-
		KollisionPunkt.get(3).x = object.x;
		KollisionPunkt.get(3).y = object.y+disty;
		
		// Rotate
		for ( int i = 1; i < 4; i++)
		{
			Point p1 = KollisionPunkt.get(0);
			Point p2 = KollisionPunkt.get(i);
			double x = Math.abs(p2.x - p1.x);
			double y = Math.abs(p2.y - p1.y);
			
			double x1 = x * Math.cos( angle) - y * Math.sin(angle);
			double y1 = x * Math.sin(angle) + y * Math.cos(angle);
			
			p2.x = (int)(p1.x + x1);
			p2.y = (int)(p1.y + y1);
		}
		if ( !center )
		{
			return;
		}
		// translate axis x
		Point p1 = KollisionPunkt.get(0);
		Point p2 = KollisionPunkt.get(1);
		Point p3 = KollisionPunkt.get(3);
		
		double x = (p2.x - p1.x);
		double y = (p2.y - p1.y);
		
		
		
		double x1 = -distx/2 / Math.sqrt( 1 + Math.pow((y/x),2) );
		double y1 = x1 * y/x;
		if ( Math.abs(angle) > Math.PI / 2)
		{
			x1 = -x1;
			y1 = -y1;
		}
		
		for ( Point p : KollisionPunkt)
		{
			p.x += x1;
			p.y += y1;
		}
		
		// translate axis y
		x1 = p3.x - p1.x;
		y1 = p3.y - p1.y;
		
		double frac = (double)-disty/distx;
		x1=frac*x1;
		y1=frac*y1;
		for ( Point p : KollisionPunkt)
		{
			p.x += x1;
			p.y += y1;
		}
	
	}
	
}
