package panzerkamp.main;

public class Constans {
		public static String LAYER_TERRAIN = "terrain";
		public static String LAYER_TERRAIN2 = "terrain2";
		public static String LAYER_SKY = "sky";
		public static String LAYER_TANKS = "tanks";
		public static String LAYER_UI = "ui";
		public static String LAYER_BUTTONS = "buttons";
		public static String LAYER_BUTTONS2 = "buttons2";
		public static String LAYER_BULLETS = "bullets";
		
		public static String MSG_WIN = "msg_win";
		public static String MSG_LOST = "msg_lost";
		public static String MSG_HIT = "msg_hit";
		public static String MSG_BOOM = "msg_boom";
		public static String MSG_CREATE = "msg_create";
		public static String MSG_DESTROY = "msg_destroy";
		public static String MSG_MOVE = "msg_move";
		public static String MSG_ROTATE = "msg_rotate";
		public static String MSG_SCALE = "msg_scale";
		public static String MSG_SHOT = "msg_shot";
		
		
		public static String MSG_OBJECT_ISBLOCKABLE = "msg_object_isblockable";
		public static String MSG_OBJECT_ISDESTROYABLE = "msg_object_isdestroyable";
		
		public static String PATH_GRAPHICS = "graphics/";
		public static String PATH_SOUND = "sounds/";
		public static String PATH_BIN = "bin/";
		public static String PATH_GRAPHICS_TANKS = "graphics/tanks/";
		public static String PATH_GRAPHICS_TERRAIN = "graphics/terrain/";
		public static String PATH_GRAPHICS_OTHER = "graphics/other/";
                
                public static int SIDE_PLAYER = 1;
                public static int SIDE_ENEMY = 1;
}
