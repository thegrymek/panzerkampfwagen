package panzerkamp.object.bulleteinfache;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Image;
import javax.swing.ImageIcon;
import maybach.object.Shape;
import panzerkamp.main.Constans;
import panzerkamp.object.bullet.Kugel;

public class EinFacheKugel extends Kugel {

    public EinFacheKugel(int x, int y, double winkel, int seite, double distanz, double kraft) {
        // 40 = this.bewegungsGeschwindigkeit;
        super(x, y, winkel, seite, 40, distanz, kraft);

        this.bewegtbild = new Image[]{
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_OTHER.concat("bullet1.png"))).getImage(),
        };

        this.updateDist(this.bewegtbild[0]);
    }

    @Override
    public Shape clone(int x, int y, double winkel, int seite) {
        return new EinFacheKugel(x, y, winkel, seite, this.distanz, this.kraft);
    }
}
