/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package panzerkamp.menu;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * @author thegrymek
 */
public class Activator implements BundleActivator {
    
    Menu menu;
    public void start(BundleContext context) throws Exception {
        this.menu = new Menu(context);
        context.registerService(Menu.class.getName(), this.menu, null);
    }
    
    public void stop(BundleContext context) throws Exception {
        this.menu.stop(context);
    }
    
}
