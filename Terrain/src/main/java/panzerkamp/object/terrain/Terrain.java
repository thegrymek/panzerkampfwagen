/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.object.terrain;

import java.awt.Image;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import maybach.engine.Engine;
import maybach.object.Shape;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import panzerkamp.main.Constans;

/**
 *
 * @author thegrymek
 */
public class Terrain extends Shape implements ITerrain {

    protected Image img = null;
    protected String mapName = null;
    
    // start tank position 
    protected int sx = 0;

    @Override
    public int getSx() {
        return sx;
    }

    @Override
    public void setSx(int sx) {
        this.sx = sx;
    }

    @Override
    public int getSy() {
        return sy;
    }

    @Override
    public void setSy(int sy) {
        this.sy = sy;
    }
    protected int sy = 0;
    
    public Terrain(String name, String imagefile, String mapName) {
        this.img = new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TERRAIN + imagefile)).getImage();
        this.name = name;
        this.mapName = mapName;
    }

    @Override
    public Image getLogo() {
        return this.img;
    }

    @Override
    public void setLogo(Image img) {
        this.img = img;
    }

    @Override
    public boolean setMap() {
        Engine e = Engine.getEngine();
        if (e == null) {
            return false;
        }
        if (this.mapName == null) {
            return false;
        }

        this.read(this.mapName, e);
        return true;
    }

    @Override
    public Map<String, String> getInfo() {
        HashMap<String, String> info = new HashMap<String, String>();

        info.put("mapName", this.mapName);
        info.put("name", this.getName());
        return info;
    }
    
    @Override
    public void read(String filename, Engine engine) {

        try {

            InputStream fXmlFile = this.getClass().getClassLoader().getResourceAsStream(filename);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("kobold");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    registerPlain(engine, eElement, Constans.LAYER_TERRAIN);
                }
            }

            nList = doc.getElementsByTagName("himmel");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    registerPlain(engine, eElement, Constans.LAYER_SKY);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getTagValue(String sTag, Element eElement) {
        try {
            NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

            Node nValue = (Node) nlList.item(0);

            return nValue.getNodeValue();
        } catch (Exception e) {
            return "null";
        }
    }

    private void registerPlain(Engine e, Element eElement, String layerName) {
        String graphics_path = getTagValue("grafik", eElement);
        int x = Integer.parseInt(getTagValue("posX", eElement));
        int y = Integer.parseInt(getTagValue("posY", eElement));
        boolean isBlockAble = false;
        boolean isDestroyAble = false;
        int gesundheit = 0;

        if (getTagValue("beweglich", eElement).equalsIgnoreCase("falsch")) {
            isBlockAble = true;
        }

        if (getTagValue("istzerstoren", eElement).equalsIgnoreCase("wahr")) {
            isDestroyAble = true;
        }

        if (!getTagValue("lebenspunkte", eElement).equalsIgnoreCase("null")) {
            gesundheit = Integer.parseInt(getTagValue("lebenspunkte", eElement));

        }
        URL path = this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TERRAIN + graphics_path);
        e.RegisterShape(new PlainSquare(x, y, path, isBlockAble, isDestroyAble, gesundheit), layerName);
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

}
