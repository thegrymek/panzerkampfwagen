package panzerkamp.object.tankpanzeriv;

import java.awt.Image;

import javax.swing.ImageIcon;

import panzerkamp.main.Constans;
import panzerkamp.object.bulleteinfache.EinFacheKugel;
import panzerkamp.object.tank.Tank;

public class PanzerIV extends Tank {

    public PanzerIV(int x, int y, double winkel, int seite) {
        super(x, y, winkel, seite);

        this.name="PanzerIV";
        // reload speed
        this.ladenGeschwindikeit = 70;
        // fire speed
        this.feuerGeschwindigkeit = 800;
        // rotate speed
        this.drehungGeschwindigkeit = 0.1;
        // move speed
        this.bewegungsGeschwindigkeit = 20;
        // health
        this.gesundheit = 3000;
        this.maxgesundheit = 3000;
        // fire strength
        this.feuerKraft = 4000;
        // shot distance
        this.schussDistanz = 400;
        // cost
        this.preis = 100;

        // kugelType
        this.geschoss = new EinFacheKugel(this.x, this.y, 0, this.getSide(), this.schussDistanz, this.feuerKraft);
        //Sprites
        this.bewegtbild = new Image[]{
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "PanzerIV-korper.png")).getImage(),
            new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "PanzerIV-fass.png")).getImage()
        };

        this.logo = new ImageIcon(this.getClass().getClassLoader().getResource(Constans.PATH_GRAPHICS_TANKS + "PanzerIV.png")).getImage();
        

        this.updateDist(this.bewegtbild[0]);
    }

}
