/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panzerkamp.game;

import maybach.engine.Engine;
import panzerkamp.object.tank.Tank;

/**
 *
 * @author thegrymek
 */
public interface IGame {

    void RegisterPlayer(Tank panzer);

    void Start();

    void Stop();

    void UnregisterPlayer(Tank panzer);

    void bindEngine(Engine e);

    void unbindEngine();
    
}
