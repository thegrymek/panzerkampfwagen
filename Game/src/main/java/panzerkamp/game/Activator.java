package panzerkamp.game;

import maybach.engine.Engine;
import maybach.layer.ILayer;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    private Game game;

    public void start(BundleContext context) throws Exception {
        this.game = new Game(context);
        ServiceRegistration ref = context.registerService(IGame.class.getName(), game, null);
    }

    public void stop(BundleContext context) throws Exception {
        // cleaning registered layers and shapes in there
        Engine e = Engine.getEngine();
        e.Stop();

    }

}
