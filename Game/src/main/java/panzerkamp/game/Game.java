package panzerkamp.game;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import maybach.layer.Layer;
import maybach.engine.*;
import maybach.layer.ILayer;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import panzerkamp.main.Constans;
import panzerkamp.object.tank.Tank;

//@Component(
//        name = "Game",
//        enabled = true,
//        metatype = true)
//@Service
public class Game implements IGame {

//    @Reference
    private Engine engine;
    private boolean isPlayer = false;
    private int lastSide = 10;
    private Tank player = null;
    private BundleContext ctx;

    public Game(BundleContext ctx) {
        this.ctx = ctx;
        // loading Game
        this.engine = Engine.getEngine();
        // loading layers
        this.engine.RegisterLayer(new Layer(Constans.LAYER_UI));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_BUTTONS));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_BUTTONS2));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_TERRAIN));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_TERRAIN2));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_BULLETS));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_TANKS));
        this.engine.RegisterLayer(new Layer(Constans.LAYER_SKY));
    }

    @Override
    public void bindEngine(Engine e) {
//        System.out.println("bind engine");
        this.engine = e;
    }

    @Override
    public void unbindEngine() {
//        System.out.println("unbind engine");
        this.engine = null;
    }

//    @Activate
    @Override
    public void Start() {
        JFrame frame = new JFrame("PanzerKampWagen Game");
        frame.setVisible(true);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                try {
                    Stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        frame.add(this.engine);

        this.engine.Start();
    }

//    @Deactivate
    @Override
    public void Stop() {
        this.engine.Stop();
        Engine e = this.engine;
        this.UnregisterPlayer(this.player);
        this.clean();

    }

    private void clean() {
        Engine e = this.engine;
        e.Stop();
        for (int i = 0; i < 100; i++) {
            ILayer l = e.getLayer(i);
            if (l == null) {
                continue;
            }

            // remove shapes from layer
            l.clear();
        }
    }

    @Override
    public void RegisterPlayer(Tank panzer) {
        Engine e = this.engine;
        if (this.isPlayer) {
            return;
        }
        e.addKeyListener(panzer);
        e.addMouseMotionListener(panzer);
        e.setShapeFocus(panzer);
        e.RegisterShape(panzer, Constans.LAYER_TANKS);
        this.isPlayer = true;
        this.player = panzer;
    }

    @Override
    public void UnregisterPlayer(Tank panzer) {
        Engine e = this.engine;
        if (this.isPlayer == false) {
            return;
        }
        e.removeMouseListener(panzer);
        e.removeKeyListener(panzer);
        e.removeMouseMotionListener(panzer);
        e.setShapeFocus(null);
        e.UnregisterShape(panzer);
        this.isPlayer = false;
        this.player = null;
    }

}
