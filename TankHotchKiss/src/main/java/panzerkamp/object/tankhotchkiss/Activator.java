package panzerkamp.object.tankhotchkiss;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import panzerkamp.main.Constans;
import panzerkamp.object.tank.Tank;

public class Activator implements BundleActivator {

    private Hotchkiss tHotchkiss;
    private BundleContext ctx;

    public void start(BundleContext context) throws Exception {
        this.tHotchkiss = new Hotchkiss(0,0,0, Constans.SIDE_PLAYER);
        this.ctx = context;
        ctx.registerService(Tank.class.getName(), this.tHotchkiss, null);
    }
    
    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
