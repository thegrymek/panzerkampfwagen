PanzerKampfWagen
----------------

_PanzerKampfWagen_ is another example of Java2D mixed with OSGI framework. 
_Java2D_ is used for rendering whole map, tanks, bullets and animations.
_OSGI_ starts the application. It also searches for tanks and maps in bundle folder and adds them in the menu window. 

Main idea were taken from second world war. Our fascination to tanks and their history had turned into work. 
We tried to reflect our tanks and terrain with reality. Check out how it looks like!

Project had been written together with my friends as project for pass studies ;)

Instalation
-----------

```html
git clone https://thegrymek@bitbucket.org/thegrymek/panzerkampfwagen.git
cd panzerkampfwagen
mvn install pax:provision
```

Example usage
-------------

After menu window starts, you have to click on tank and map. Once you do it, click button to play. 

You can choose tanks:
[Panther](http://en.wikipedia.org/wiki/Panther_tank) 
[T34/85](http://en.wikipedia.org/wiki/T-34)
[Panzer IV](http://en.wikipedia.org/wiki/Panzer_IV)
[Hotch Kiss](http://en.wikipedia.org/wiki/Hotchkiss_H35)

License
-------

_PanzerKampfWagen_ is licensed as Free for Usage & Distributing.